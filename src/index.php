<?php
$url = $_SERVER['REQUEST_URI'];
$method =$_SERVER["REQUEST_METHOD"];


switch ($method) {
    case 'GET':

        require('module/header.php' );

        switch ($url) {
            case '/':   
                require('module/main.php' );
                break;
            default:
                echo "page 404";
                break;
        }
        require('module/footer.php' );
        break;

    case 'POST':
        switch ($url) {
        case '/probation':   
            require('module/probation.php' );
            break;
        case '/user-dismission':   
            require('module/user-dismission.php' );
            break;
        case '/last_employee_chief':   
            require('module/last_employee_chief.php' );
            break;
        default:
            echo "page 404";
            break;
    }
    
    break;
}

?>