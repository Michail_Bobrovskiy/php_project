<main>
<section class="humanDepartment">
    <h1 class="humanDepartment__header">Отдел кадров</h1>
    <div class="humanDepartment__controllers">
        <label for="humanDepartment-1" class="humanDepartment__controller">Испытательный срок</label>
        <label for="humanDepartment-2" class="humanDepartment__controller">Уволенные</label>
        <label for="humanDepartment-3" class="humanDepartment__controller">Последний нанятый сотрудник</label>
        <input name="humanDepartment" type="radio" id='humanDepartment-1' class="humanDepartment__controller humanDepartment__controller-hidden">
        <input name="humanDepartment" type="radio" id='humanDepartment-2' class="humanDepartment__controller humanDepartment__controller-hidden">
        <input name="humanDepartment" type="radio" id='humanDepartment-3' class="humanDepartment__controller humanDepartment__controller-hidden">
    </div>
    <table class="humanDepartment__data">
    <tr class="humanDepartment__row">
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>

    </tr>
    <tr class="humanDepartment__row">
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>

    </tr>
    <tr class="humanDepartment__row">
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>

    </tr>
    <tr>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>
        <td class="humanDepartment__cell"></td>

    </tr>
    </table>
    <div class="humanDepartment__controllers humanDepartment__controllers-data">
        
    </div>
</section>
</main>