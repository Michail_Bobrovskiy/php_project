<?php 
require_once('db/connection.php');

$sql = "SELECT user.last_name, user.middle_name FROM `user` JOIN `user_position` as up1 ON user.id = up1.user_id LEFT JOIN `user_dismission` ON user_dismission.user_id = user.id WHERE (user_dismission.is_active != 1 OR user_dismission.is_active IS NULL) AND up1.created_at = (SELECT MAX(up2.created_at) FROM user_position as up2 WHERE up2.department_id = up1.department_id AND up2.position_id NOT IN (SELECT leader_id FROM department)) GROUP BY up1.department_id";
$connect = new Connection();

$query = $connect->query($sql);
$result = [];
        while ($row = $query->fetch_assoc()) {
            $result[] = $row["last_name"].' '.$row["first_name"].' '.$row["middle_name"];
        }


echo json_encode($result, JSON_UNESCAPED_UNICODE);

?>