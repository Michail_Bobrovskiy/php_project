<?php 
require_once('db/connection.php');
$sql = "SELECT user.first_name, user.last_name,user.middle_name FROM `user` JOIN `user_position` ON user.id = user_position.user_id JOIN `user_dismission` ON user_position.user_id <> user_dismission.user_id WHERE TO_DAYS(DATE_SUB(NOW(), INTERVAL 3 MONTH)) < TO_DAYS(user_position.created_at) AND user_dismission.is_active = 1";
$connect = new Connection();

$query = $connect->query($sql);
$result = [];
        while ($row = $query->fetch_assoc()) {
            $result[] = $row["last_name"].' '.$row["first_name"].' '.$row["middle_name"];
        }

sort($result);

echo json_encode($result, JSON_UNESCAPED_UNICODE);

?>