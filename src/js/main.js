
let dataCell;
let page = 1;
let pageAll;
function renderCell(dataCell,page=1) {
	let  from = (page -1) * 16;
	let cell = document.querySelectorAll('.humanDepartment__cell');
	let j=0;
	pageAll = Math.ceil(dataCell.length / 16);
		for(let i=from;i<16*page && dataCell.length;i++) {
			cell[j].textContent = dataCell[i];
			j++;
		}
		if(pageAll>1) {
			
			let controller = document.querySelector('.humanDepartment__controllers-data');
			controller.innerHTML='';
			controller.innerHTML += `<span class="humanDepartment__controller humanDepartment__controller-prev" onclick="pageTrigger('prev')">Назад</span>`;
			for (let i=1;i<=pageAll; i++) {
			controller.innerHTML += `<span class="humanDepartment__controller humanDepartment__controller-page controller"onclick="pageTrigger(${i})">${i}</span>`	
			}
			controller.innerHTML += `<span class="humanDepartment__controller humanDepartment__controller-next controller"onclick="pageTrigger('next')">Вперед</span>`
		}
	
		
}
function pageTrigger(count) {
	
	
	if (count=='prev' && page>1) {
		page--;
		renderCell(dataCell,page);
		
		
		return
	}
	if (count=='next' && page<pageAll) {
		page++;
		renderCell(dataCell,page);
		
		
		return
	} 
	if (typeof(count)=="number") {
		page = count;
		renderCell(dataCell,page);
		return
	}
}
function ajax(path) {
	$.ajax({
		url:path,
		type:'POST',
		dataType:'json',
		data: {
		},
		success (data) {
			dataCell = data;
			// let arr = [1,2,3,4,5,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,,7,7,7,7,9];
			// dataCell.push(...arr); для тестирования пагинации
			
			renderCell(dataCell);

		}
	})
}



	$('#humanDepartment-1').change(function(e){
		ajax('./probation');
	})
	$('#humanDepartment-2').change(function(e){
		ajax('./user-dismission');
	})
	$('#humanDepartment-3').change(function(e){
		ajax('./last_employee_chief');
	})
	