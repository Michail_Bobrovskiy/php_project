const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const browserSync = require('browser-sync').create();

const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const gulpif = require('gulp-if');
const gcmq = require('gulp-group-css-media-queries');
const less = require('gulp-less');

const isDev = (process.argv.indexOf('--dev') !== -1);
const isProd = !isDev;
const isSync = (process.argv.indexOf('--sync') !== -1);


function styles() {
	return gulp.src('./src/css/style.less')
		.pipe(gulpif(isDev, sourcemaps.init()))
		.pipe(less())
		// .pipe(concat('style.css'))
		.pipe(gcmq())
	   	.pipe(autoprefixer({ 
	   		overrideBrowserslist:['>0.1%'],
            cascade: false
        })) /* установит автопрефиксы для поддержки браузеров */
       // .on('error',console.error.bind(console))
	   	.pipe(gulpif(isProd,cleanCSS({
	   		level:2
	   	})))
	   	.pipe(gulpif(isDev, sourcemaps.write()))
	  	.pipe(gulp.dest('./build/css'))
	  	.pipe(browserSync.stream());
		
}


function img() {
	return gulp.src('./src/img/**/*')
	.pipe(gulp.dest('./build/img'))
}

function html() {
	return gulp.src('./src/**/*.php')
	.pipe(gulp.dest('./build'))
	.pipe(browserSync.stream());
}

function js() {
	return gulp.src('./src/js/*.js')
	.pipe(gulp.dest('./build/js'))
	.pipe(browserSync.stream());
}
function libs() {
	return gulp.src('./src/libs/*.js')
	.pipe(gulp.dest('./build/libs'))
	.pipe(browserSync.stream());
}
function htaccess() {
	return gulp.src('./src/*.htaccess')
	.pipe(gulp.dest('./build'))
	.pipe(browserSync.stream());
}
function clear() {
	return del('build/*');
}
function watch() {
	if(isSync) {
	 browserSync.init({
        
	 	proxy:"testPhp.dev" // change me
        
    });
	 }
	gulp.watch('./src/css/**/*.less',styles);
	gulp.watch('./src/*.php',html);
	gulp.watch('./src/*.htaccess',htaccess);
	gulp.watch('./src/js/*.js',js);
	gulp.watch('./src/libs/*.js',libs);

}

let build = gulp.series(clear,
 gulp.parallel(styles, img, html, htaccess,js,libs)
);

 gulp.task('build', build); 
  gulp.task('watch', gulp.series(build, watch)); 

