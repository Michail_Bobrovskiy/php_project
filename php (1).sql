-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 28 2021 г., 08:26
-- Версия сервера: 5.5.50
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `php`
--

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL,
  `leader_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_ad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`id`, `leader_id`, `name`, `description`, `created_ad`, `update_at`) VALUES
(1, 1, 'Отдел безделья', 'пить чай и ни чего не делать', '2021-06-27 18:05:25', '0000-00-00 00:00:00'),
(2, 3, 'Отдел разработки', 'писать коt', '2021-06-27 18:08:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `dismission_reason`
--

CREATE TABLE IF NOT EXISTS `dismission_reason` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dismission_reason`
--

INSERT INTO `dismission_reason` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'отсутсвие на рабочем месте', 'Выпал из окна курилки второй раз за месяц', '2021-06-27 17:03:00', '0000-00-00 00:00:00'),
(2, 'прогулы', 'Второй год не выходит из отпуска\r\n', '2021-06-27 17:11:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `salary` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position`
--

INSERT INTO `position` (`id`, `name`, `description`, `salary`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Начальник', 'Начальничать', 100500, '2021-06-27 16:55:05', '0000-00-00 00:00:00', 1),
(2, 'раб', 'быть рабом', 0, '2021-06-27 16:56:39', '0000-00-00 00:00:00', 1),
(3, 'Начальник2', 'Руководить', 500100, '2021-06-27 18:08:35', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `middle_name` varchar(45) NOT NULL,
  `data_of_birth` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `middle_name`, `data_of_birth`, `created_at`, `update_at`) VALUES
(1, 'Михаил', 'Бобровский', 'Сергеевич', '0000-00-00', '2021-06-27 16:34:47', '0000-00-00 00:00:00'),
(2, 'Михаил2', 'Бобровский2', 'Сергеевич3', '0000-00-00', '2021-06-27 16:36:06', '0000-00-00 00:00:00'),
(3, 'Михаил3', 'Бобровский3', 'Сергеевич3', '0000-00-00', '2021-06-27 16:48:51', '0000-00-00 00:00:00'),
(4, 'Михаил4', 'Бобровский4', 'Сергеевич4', '0000-00-00', '2021-06-27 16:49:06', '0000-00-00 00:00:00'),
(5, 'Михаил5', 'Бобровски5й', 'Сергеевич5', '0000-00-00', '2021-06-27 16:49:17', '0000-00-00 00:00:00'),
(6, 'Михаил6', 'Бобровский6', 'Сергеевич6', '0000-00-00', '2021-06-27 16:49:25', '0000-00-00 00:00:00'),
(7, 'Начальник1', 'Бобровский', 'Сергеевич', '0000-00-00', '2021-06-27 16:49:46', '0000-00-00 00:00:00'),
(8, 'Начальник2', 'Бобровский', 'Сергеевич', '0000-00-00', '2021-06-27 16:49:53', '0000-00-00 00:00:00'),
(9, 'Михаил7', 'Бобровский', 'Сергеевич', '0000-00-00', '2021-06-27 17:34:09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `user_dismission`
--

CREATE TABLE IF NOT EXISTS `user_dismission` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reason_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_dismission`
--

INSERT INTO `user_dismission` (`id`, `user_id`, `reason_id`, `is_active`, `created_at`, `update_at`) VALUES
(1, 2, 1, 1, '2021-06-27 17:41:34', '0000-00-00 00:00:00'),
(2, 3, 2, 0, '2021-06-27 17:41:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `user_position`
--

CREATE TABLE IF NOT EXISTS `user_position` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_position`
--

INSERT INTO `user_position` (`id`, `user_id`, `department_id`, `position_id`, `created_at`, `update_at`) VALUES
(11, 1, 2, 2, '2021-06-27 18:11:04', '0000-00-00 00:00:00'),
(12, 2, 2, 2, '2020-09-21 18:16:58', '0000-00-00 00:00:00'),
(13, 3, 2, 2, '2021-06-27 18:17:08', '0000-00-00 00:00:00'),
(14, 4, 2, 2, '2021-06-27 18:17:17', '0000-00-00 00:00:00'),
(15, 5, 1, 2, '2021-06-27 18:17:31', '0000-00-00 00:00:00'),
(16, 6, 1, 2, '2021-06-27 18:17:43', '0000-00-00 00:00:00'),
(17, 9, 1, 2, '2021-06-27 18:17:53', '0000-00-00 00:00:00'),
(18, 7, 1, 1, '2021-06-27 18:18:12', '0000-00-00 00:00:00'),
(19, 8, 2, 3, '2021-06-27 18:18:30', '0000-00-00 00:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_leader_id_index` (`leader_id`);

--
-- Индексы таблицы `dismission_reason`
--
ALTER TABLE `dismission_reason`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_dismission`
--
ALTER TABLE `user_dismission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_dismission_reason_id_index` (`reason_id`),
  ADD KEY `user_dismission_user_id_index` (`user_id`);

--
-- Индексы таблицы `user_position`
--
ALTER TABLE `user_position`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_position_department_id_index` (`department_id`),
  ADD KEY `user_position_position_id_index` (`position_id`),
  ADD KEY `user_position_user_id_index` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `dismission_reason`
--
ALTER TABLE `dismission_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `position`
--
ALTER TABLE `position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `user_dismission`
--
ALTER TABLE `user_dismission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `user_position`
--
ALTER TABLE `user_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_position_id_fk` FOREIGN KEY (`leader_id`) REFERENCES `position` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_dismission`
--
ALTER TABLE `user_dismission`
  ADD CONSTRAINT `user_dismission_dismission_reason_id_fk` FOREIGN KEY (`reason_id`) REFERENCES `dismission_reason` (`id`),
  ADD CONSTRAINT `user_dismission_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_position`
--
ALTER TABLE `user_position`
  ADD CONSTRAINT `user_position_department_id_fk` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `user_position_position_id_fk` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`),
  ADD CONSTRAINT `user_position_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
